<?php

return [
	'active'		=> 'Active',

	// form
	'selectCategory'	=> 'Select Category',
	'challengeName'		=> 'Challenge Name',
	'description'		=> 'Description',
	'maxAttempt'		=> 'Max Attempt',
	'type'				=> 'Type',
	'status'			=> 'Status',

	'addChallenge'		=> 'Add Challenge',
	'editChallenge'		=> 'Edit Challenge',
	'updateChallenge'	=> 'Update Challenge',
	'deleteChallenge'	=> 'Detele Challenge',

	'active'			=> 'Active',
	'passive'			=> 'Passive',
	'static'			=> 'Static',
	'dynamic'			=> 'Dynamic',

	'flags'				=> 'Flags',
	'regex'				=> 'Regex',
	'content'			=> 'Content',
	'browse'			=> 'Browse',

	'noCategoryWarning'	=> [
		'title' => "Warning",
		'content'	=> 'There is no category. Go and create a new one!'
	],

	'fileUploadSuccessful' => 'File uploaded Successfully',
	'FileUploadError'      => 'There was an error while upload successfully',
	'fileUploadPhpError'   => 'Php files newer allowed!',
];