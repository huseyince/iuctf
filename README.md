# IUCTF

A ctf platform for everyone.

## Why We did this ?

Because

* We wanted to do something about cyber security.
* We wanted something with codeigniter 4 (Because codeigniter 4 awesome)
* Just for fun :)

If you like our project, please ...

* Create pull request.
* Share with your friends.

If you don't like or hate

* Feel free not to use it.
* Or show us how to do it better.

## Installation

First steps:

```bash
$ git clone https://gitlab.com/iucyber/iuctf.git
$ cd iuctf
$ composer install
```

You must install these php extensions. These extensions need for codeigniter 4.

```
php-curl
php-intl
php-mbstring
php-zip
php-mysql
php-xml
```

Now, serve this project via spark.

```bash
$ php spark serve
```

Go to http://localhost:8080/install and then finish installation.

**Note:** More options on [wiki](https://gitlab.com/iucyber/iuctf/-/wikis/Installation)

## LICENSE

This Project under MIT license.